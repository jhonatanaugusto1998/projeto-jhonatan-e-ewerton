from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('home.html', title='Home', content='Hello, World!')

@app.route('/about')
def about():
    return render_template('about.html', title='About', content='About Us')

app.config['DEBUG'] = True

if __name__ == '__main__':
    app.run(debug=True)

